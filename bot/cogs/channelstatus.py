from discord.ext.commands import Bot, command
from discord import Member, Message

from bot.constants import Channels
from bot.decorators import with_role

# Channels to check
ALLOWED_CHANNELS = (
    Channels.help_0,
    Channels.help_1,
    Channels.help_2,
    Channels.help_3,
    Channels.help_4,
    Channels.help_5
)

# Roles that can override channel status
ALLOWED_ROLES = (Roles.owner, Roles.admin, Roles.moderator, Roles.helpers)

# Dict for mode lookup
MODES = {
    "busy": "⛔",
    "away": "⏳",
    "free": "✅"
}

# List of start regexes (e.g. "How do I ...")
HELP_START = [
    r"how \w+ i",
    r"help me",
    r"for me\?",
    r"could \w+ (explain | tell me)",
    r"(doesn'?t | not) work",
    r"don'?t (know | get)"
]

HELP_START_COMPILED = [re.compile(rule, re.I) for rule in HELP_START]

# List of end regexes (e.g. "Thank you")
HELP_END = [
    r"ok",
    r"thanks",
    r"thank you",
    r"now it works",
    r"it works now"
]

HELP_END_COMPILED = [re.compile(rule, re.I) for rule in HELP_END]

class ChannelStatus:
    def __init__(self, bot: Bot):
        self.bot = bot
        self.channels = {}  # Dict[Int, Int]  -- UserID: ChannelID
        self.last_seen = {}  # Dict[Int, Datetime]  -- UserID: LastMessageTimeStamp

    # Checking functions
    def invalid_channel(self, message: Message) -> bool:
        return message.channel.id not in ALLOWED_CHANNELS

    def is_start(self, message: Message) -> bool:
        return any(rgx.search(message.content) for rgx in HELP_START_COMPILED)

    def is_end(self, message: Message) -> bool:
        return any(rgx.search(message.content) for rgx in HELP_END_COMPILED)

    async def on_message(self, message: Message):
        if message.author.bot or self.invalid_channel(message):
            return

        # Keep track of users being here or not
        if message.author.id in self.last_seen:
            self.last_seen[message.author.id] = message.created_at

        if self.is_start(message):
            if message.author.id in self.channels:
                # User already has their own channel
                return

            elif message.channel.id in self.channels.values():
                # Redirect to other channel if one is available
                available_channels = [channel for channel in ALLOWED_CHANNELS
                                      if channel not in self.channels.values()]

                if available_channels:
                    await ctx.send(f"<@{message.author.id}> this channel is currently in use. Try <#{available_channels[0]}> instead.")

                else:
                    # TODO: All channels are busy. Send the user a message to wait for a while?
                    return

            else:
                # Channel and user are both not in use
                channel = self.channels[message.author.id]
                name = channel.name[1:] if channel.name.startswith("✅") else channel.name  # Filter out emojis
                await channel.edit(name=f"⛔{name}")
                self.channels[message.author.id] = message.channel.id
                self.last_seen = message.created_at
                await ctx.send("Please be patient when asking a question, not all helpers are available at all times. I've marked this channel as In Use for you.")

        elif self.is_end(message):
            if message.author.id in self.channels:
                channel = self.channels[message.author.id]
                name = channel.name[1:] if channel.name.startswith("⛔") else channel.name  # Filter out emojis
                await channel.edit(name=f"✅{name}")
                del self.channels[message.author.id]
                del self.last_seen[message.author.id]

        else:
            # TODO: Wrap this in a timed background task?
            now = datetime.datetime.utcnow()
            for user, timestamp in self.last_seen.items():
                if (now - timestamp).minutes >= 30:
                    channel = self.channels[user]
                    name = channel.name[1:] if channel.name.startswith("⛔") else channel.name  # Filter out emojis
                    await channel.edit(name=f"⏳{name}")
                    del self.channels[user]
                    del self.last_seen[user]

    @with_role(*ALLOWED_ROLES)
    @command()
    async def setchannelstatus(self, ctx, status: str, user: Member = None):
        if status.lower() not in MODES:
            await ctx.send("Invalid status, must be one of: {', '.join(MODES.keys())}")

        elif user.id not in self.channels and status == "free":
            await ctx.send("User does not have their own channel!")

        elif ctx.channel.id not in self.channels.values() and status == "free":
            await ctx.send("Channel is not in use!")

        # TODO: check more possibilities; potentially redo this entire function.

        else:
            name = ctx.channel.name[1:] if any(ctx.channel.name.startswith(c) for c in MODES.values())else ctx.channel.name 
            await ctx.channel.edit(name=f"{MODES[status]}{name}")
            await ctx.send("Updated channel!")
            # TODO: add/remove self.channels and self.last_seen

def setup(bot: Bot):
    bot.add_cog(ChannelStatus(bot))